# import the packages needed for creating s3 bucket using CDK
from aws_cdk import Stack
from aws_cdk import aws_s3 as s3
from constructs import Construct


class Ids721Mp3Stack(Stack):
     def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # create s3 bucket using CDK and Add bucket properties like versioning and encryption
        s3.Bucket(self, "MyBucket",
                  versioned=True,
                  encryption=s3.BucketEncryption.KMS_MANAGED)