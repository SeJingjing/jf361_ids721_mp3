import aws_cdk as core
import aws_cdk.assertions as assertions

from ids721_mp3.ids721_mp3_stack import Ids721Mp3Stack

# example tests. To run these tests, uncomment this file along with the example
# resource in ids721_mp3/ids721_mp3_stack.py
def test_sqs_queue_created():
    app = core.App()
    stack = Ids721Mp3Stack(app, "ids721-mp3")
    template = assertions.Template.from_stack(stack)

#     template.has_resource_properties("AWS::SQS::Queue", {
#         "VisibilityTimeout": 300
#     })
